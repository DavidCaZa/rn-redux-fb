import React, { Component } from 'react';
import { AppRegistry } from 'react-native'
import { Provider } from 'react-redux';
import store from './app/redux/store'
import AppNavigator from './app/AppNavigator';

export default class App extends Component {
   render() {
     return(
    <Provider store={store}>
      <AppNavigator />
     </Provider>
     )
   }
 }

 AppRegistry.registerComponent('evaluacion', () => App);

    
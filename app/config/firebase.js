import firebase from 'firebase';
var firebaseConfig = {
  apiKey: "**************************************",
  authDomain: "******************************",
  databaseURL: "https://bdventasfb.firebaseio.com",
  projectId: "bdventasfb",
  storageBucket: "bdventasfb.appspot.com",
  messagingSenderId: "454598291377",
  appId: "1:454598291377:web:1c381741ede9c8d1"
};
firebase.initializeApp(firebaseConfig);

export default firebase;
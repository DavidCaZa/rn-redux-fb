import React, { Component } from 'react';
import { createStackNavigator,createBottomTabNavigator,createSwitchNavigator, createAppContainer } from 'react-navigation';
import { FontAwesome } from "react-native-vector-icons";

import LogIn from './component/login/LogIn'

import HomeScreen from './component/screens/HomeScreen'
import ProductsScreen from './component/screens/ProductsScreen';
import AwardsScreen from './component/screens/AwardsScreen';


const RootStack = createStackNavigator(
  {
    Inicio: {
      screen: LogIn,
      headerMode: 'none',
      header: null,
      navigationOptions: {
        header: null
      }  
    }
  },
  {
    initialRouteName: 'Inicio'
  }
);

const RootBottomTab = createBottomTabNavigator({
  DashBoard: {
      screen:HomeScreen,
      navigationOptions: {
      tabBarIcon: ({tintColor}) => 
      <FontAwesome name="user" size={25} color={tintColor} />
      }
  },
  Productos: {
      screen:ProductsScreen,
      navigationOptions: {
          tabBarIcon: ({tintColor}) =>
          <FontAwesome name="home" size={25} color={tintColor} />
      }
  },
  Premios: {
      screen:AwardsScreen,
      navigationOptions: {
          tabBarIcon: ({tintColor}) =>
          <FontAwesome name="shopping-cart" size={25} color={tintColor} />
      }
  } 
},
{
tabBarOptions: {
  activeTintColor: 'orange',
  inactiveTintColor: 'gray'
}
}
);

const AppNavigator =  createSwitchNavigator(
    {
      SignedIn: {
        screen: RootBottomTab
      },
      SignedOut: {
        screen: RootStack
      }
    },
    {
      initialRouteName: "SignedOut"
    }
  );


const AppNavigatorManager = createAppContainer(AppNavigator);

export default AppNavigatorManager;
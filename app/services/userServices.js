import { Alert} from 'react-native';
import firebase from '../config/firebase'

class Firebase {

  userLogin = (email, password) => {
    return new Promise((resolve,reject) => {
      firebase.auth().signInWithEmailAndPassword(email, password)
        .catch(error => {
            let message = '';
          switch (error.code) {
            case 'auth/invalid-email':
                    message='Usuario o Clave incorrecta!';
              break;
            case 'auth/user-not-found':
                    message='Usuario o Clave incorrecta!';
                    break;
            case 'auth/wrong-password':
                    message='Usuario o Clave incorrecta!';
              break;
            default:
                    message='Verificar internet';
          }
          reject(Error(message));
        }).then(user => {
        if (user) {
          resolve(user);
        }
      });
    })
  };

  getUserAuthenticate = (code) => {
    return fetch(`https://blooming-falls-18368.herokuapp.com/info_clientes/${code}`)
      .then((response) => response.json());
      
  }

  getProducts = (code) => {
    return fetch(`https://blooming-falls-18368.herokuapp.com/info_puntos/${code}`)
          .then((response) => response.json())
      
  }

  getAwards = (code) => {
    return fetch(`https://blooming-falls-18368.herokuapp.com/info_premios/${code}`)
          .then((response) => response.json())
      
  }

};

export default new Firebase();
import { _LOGOUT, _LOGIN} from './actionTypes'
import { AsyncStorage } from 'react-native'

const CURRENT_USER = 'CURRENT_USER'

export const setUser = (user) => {
  return dispatch => {
    return AsyncStorage.setItem(CURRENT_USER, JSON.stringify(user))
      .then(() => {
        dispatch({
          type: _LOGIN,
          payload: user
        })
      }).catch(() => {
        dispatch({
          type: _LOGOUT,
          payload: null
        })
      })
  }
}

export const logOut = () => { 
  return dispatch => {
    AsyncStorage.setItem(CURRENT_USER, '');
    dispatch({
      type: _LOGOUT,
      payload: null
    });
}
 }

export const fetchUser = () => {
  return dispatch => {
    return AsyncStorage.getItem(CURRENT_USER)
      .then((user) => {
        dispatch({
          type: _LOGIN,
          payload: user
        })
      }).catch(() => {
        dispatch({
          type: _LOGOUT,
          payload: null
        })
      })
  }
}


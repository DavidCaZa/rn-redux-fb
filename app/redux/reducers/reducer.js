import { _LOGOUT, _LOGIN } from '../actions/actionTypes';

const initialState = {
    isLoggedIn: undefined,
    user: null
  };

export default function(state = initialState, action) {
    switch (action.type){
        case _LOGIN:
            return {...state, isLoggedIn:true, user: action.payload };
        case _LOGOUT:
            return {...state, isLoggedIn:false,user: null };
        default:
            return state;
    }
}
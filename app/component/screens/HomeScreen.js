import React,{Component} from 'react';
import { connect } from 'react-redux';
import { Text, View, TextInput,AsyncStorage,BackHandler,Button,TouchableOpacity } from 'react-native';
import { Header } from 'react-native-elements';
import styles from "../login/style";
import HeaderCustom from '../headers/header'

class HomeScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
        socio: '',
        cuota_mantener: '',
        cuota_subir: '',
        avance: '',
        puntos: ''
    };

    this.authenticate();
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
}

componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
}

componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
}

authenticate() {
  AsyncStorage.getItem('CURRENT_USER')
      .then((user) => {
        
        if(user == null){
          this.props.navigation.navigate('Inicio');
        }else{
          let item = JSON.parse(user);
        this.setState({
          socio: item.socio,
          cuota_mantener: item.cuota_mantener,
          cuota_subir: item.cuota_subir,
          avance: item.avance,
          puntos: item.puntos
        });
        }        
      }).catch(() => {
        Alert.alert('Alert','Usuario o Clave incorrecta!');
      });
}

handleBackButtonClick() {
  this.props.navigation.goBack(null);
  return true;
}

LogOut = () => {
  AsyncStorage.clear();
  this.props.navigation.navigate("SignedOut")
}

render() {
    return (
      <View>
        
 <Header 
   leftComponent={<HeaderCustom data='' />}
   centerComponent={<HeaderCustom data='title' title='Inicio'/>}
   rightComponent={<HeaderCustom data=''/>}
   containerStyle={{
    justifyContent: 'space-between',
  }}
  />
    
      <View style={styles.container}>
          <Text style={{ color: '#0D92CA', fontSize: 15 }}>
            Categoría:
          </Text>
          <TextInput style={[styles.textInputProfile, { marginTop: 10 }]} value={this.state.socio}/>
        <Text style={{ color: '#0D92CA', fontSize: 15 }}>
           Cuota mensual:
          </Text>
        <TextInput style={[styles.textInputProfile, { marginTop: 10 }]} value={this.state.cuota_mantener.toString()}/>
          <Text style={{ color: '#0D92CA', fontSize: 15 }}>
            Cuota siguente categoría:
          </Text>
          <TextInput style={[styles.textInputProfile, { marginTop: 10 }]} value={this.state.cuota_subir.toString()}/>
          <Text style={{ color: '#0D92CA', fontSize: 15 }}>
            Avance mensual:
          </Text>
          <TextInput style={[styles.textInputProfile, { marginTop: 10 }]} value={this.state.avance.toString()}/>
          <Text style={{ color: '#0D92CA', fontSize: 15 }}>
            Puntos acumulados:
          </Text>
          <TextInput style={[styles.textInputProfile, { marginTop: 10 }]} value={this.state.puntos.toString()}/>

          <TouchableOpacity onPress={ this.LogOut } style={[styles.button]}>
          <Text style={{ color: 'white', fontSize: 20, fontWeight: '600' }}>
          { 'CERRAR SESSION'}
          </Text>
        </TouchableOpacity>
      
      </View>
      </View>
    );
  }

}




function mapStateToProps(state) {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps)(HomeScreen);


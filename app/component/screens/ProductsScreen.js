import React,{Component} from 'react';
import { connect } from 'react-redux';
import { Text, View, FlatList,Image,Button,ActivityIndicator,AsyncStorage,Alert } from 'react-native';
import {SearchBar,Header } from 'react-native-elements'
import HeaderCustom from '../headers/header'
import styles from "../login/style";
import userService from '../../services/userServices'


class ProductsScreen extends Component {

    constructor(props){
        super(props);
        this.state={
          search: '',
          productos:[],
          isLoading: false,
          user : {}
        }
        this.authenticate();
    }

    authenticate() {
      AsyncStorage.getItem('CURRENT_USER')
          .then((user) => {
            
            if(user == null){
              this.props.navigation.navigate('Inicio');
            }else{
              this.setState({user: JSON.parse(user)});
              this.props.navigation.navigate('Menu');
            }        
          }).catch(() => {
            Alert.alert('Alert','Usuario o Clave incorrecta!');
          });
    }

    

    SearchFilterFunction(text) {
      let  lenText = text.length;
      let typetext = (typeof text);
      let value = parseInt(text)
      let isInteger = Number.isInteger(value);
      this.setState({
        search: text,
        isLoading: true
      });

      if(lenText>0 && isInteger && !isNaN(value)){
        if(isInteger){
          this.getProducts(value);
        }else{
          this.setState({
            isLoading: false
          });
          Alert.alert("Debe ingresar solo números!");
        }
        
      }else{
        this.setState({
          productos: [],
          search: text,
          isLoading: false
        });
      }
    }

    getProducts(code){
      userService.getProducts(code)
      .then((responseJson) => {

        var resultado = [];

          let productfilter = responseJson.filter((value) => value.cod_cliente == this.state.user.cod_cliente || this.state.user.socio == value.cod_cliente ||  value.cod_cliente == "Todos");
          productfilter.sort(function (a, b) {	return b.puntos - a.puntos; });

         this.setState({ productos: productfilter,isLoading: false });
      }).catch((error) => {
            console.error(error);
      });
    }
  
    renderSeparator = () => {
      return (
        <View
          style={{
            height: 10,
            width: '100%',
            backgroundColor: '#CED0CE'
          }}
        />
      );
    };

    renderHeader = () => {
      return (
        <View>
        <Header
        leftComponent={<HeaderCustom data=''/>}
        centerComponent={<HeaderCustom data='title' title='Productos'/>}
        rightComponent={<HeaderCustom data=''/>}
       />
        <SearchBar
              lightTheme        
          onChangeText={text => this.SearchFilterFunction(text)}
          placeholder="Ingrese código del producto"
          value={this.state.search}
        /> 
        </View>
         
      );
    };
     
  render() {
    if(!this.state.isLoading){
      return (
        <View>
    
       <FlatList 
       ListHeaderComponent={this.renderHeader}
            
          ItemSeparatorComponent={this.renderSeparator}
            keyExtractor={(item, index) => `${index}`}
            data={this.state.productos}
            renderItem={({item}) => {
            return (
              <View style={styles.card}>
               
               <View style={styles.cardHeader}>
                  <View style={{
                                flex: 1,
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                }}>
                    <Text style={styles.title}>{`${item.nombre_producto}`}</Text>
                    <Text style={styles.time}>{`PUNTOS: ${item.puntos}`}</Text>
                  </View>
                </View>

                <View style={styles.cardImage}>
                    <Image style={{width: 150, height: 150}} source={{uri:item.Link}}/>
                </View>
                
                <View style={styles.cardFooter}>
                  <View style={styles.BarContainer}>
                    <View style={{ width: "100%", marginTop: 10 }}>
                    <Button style={styles.BarButton} bordered warning title="Comprar"/>
                    </View>
                  </View>
                </View>
              </View>
            )
          }}/>
 
        
                    
      </View>
    );
    }else{
      return (
        <View style={styles.loading}>
          <ActivityIndicator size="large" color="#00ff00" />
        </View>
      )
    }
    
  }

  search = text => {
    console.log(text);
  };
  clear = () => {
    this.search.clear();
  };

}




function mapStateToProps(state) {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps)(ProductsScreen);

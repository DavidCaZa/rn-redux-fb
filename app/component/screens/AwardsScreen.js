import React,{Component} from 'react';
import { connect } from 'react-redux';
import { Text, View, FlatList,Image,Button,ActivityIndicator ,AsyncStorage,Alert } from 'react-native';
import {SearchBar,Header } from 'react-native-elements'
import HeaderCustom from '../headers/header'
import styles from "../login/style";
import userService from '../../services/userServices'

export default class AwardsScreen extends Component {

  constructor(props){
    super(props);
    this.state={
      search: '',
      premios:[],
      isLoading: false,
      user : {}
    }
    this.authenticate();
    
  }

authenticate() {
  AsyncStorage.getItem('CURRENT_USER')
      .then((user) => {
        if(user == null){
          this.props.navigation.navigate('Inicio');
        }else{
          this.setState({user: JSON.parse(user)});
          this.getAwards();
          this.props.navigation.navigate('Menu');
        }        
      }).catch(() => {
        Alert.alert('Alert','Usuario o Clave incorrecta!');
      });
}

getAwards(){

  let category = this.state.user.socio;
let code = 0;

switch (category){
  case "Oro":
      code = 91; break;
  case "Plata":
      code = 92; break;
  case "Bronce":
      code = 93; break;
}

  userService.getAwards(code)
  .then((responseJson) => {
    let awardsfilter = responseJson.filter((value) => value.pruntos <= this.state.user.puntos);
    awardsfilter.sort(function (a, b) {	return b.pruntos - a.pruntos; });
    this.setState({ premios: awardsfilter,isLoading: false });
  }).catch((error) => {
        console.error(error);
  });
}

renderSeparator = () => {
  return (
    <View
      style={{
        height: 10,
        width: '100%',
        backgroundColor: '#CED0CE'
      }}
    />
  );
};

renderHeader = () => {
  return (
    <View>
    <Header
    leftComponent={<HeaderCustom data=''/>}
    centerComponent={<HeaderCustom data='title' title='Premios'/>}
    rightComponent={<HeaderCustom data=''/>}
   />
    </View>
     
  );
};

render() {
  if(!this.state.isLoading){
    return (
      <View>
  
     <FlatList 
     ListHeaderComponent={this.renderHeader}
          
        ItemSeparatorComponent={this.renderSeparator}
          keyExtractor={(item, index) => `${index}`}
          data={this.state.premios}
          renderItem={({item}) => {
          return (
            <View style={styles.card}>
             
             <View style={styles.cardHeader}>
                <View style={{
                              flex: 1,
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                              }}>
                  <Text style={styles.title}>{`${item.premio}`}</Text>
                  <Text style={styles.time}>{`PUNTOS: ${item.pruntos}`}</Text>
                </View>
              </View>

              <View style={styles.cardImage}>
                  <Image style={{width: 150, height: 150}} source={{uri:item.link}}/>
              </View>
              
              <View style={styles.cardFooter}>
                <View style={styles.BarContainer}>
                  <View style={{ width: "100%", marginTop: 10 }}>
                  <Button style={styles.BarButton} bordered warning title="Canjear"/>
                  </View>
                </View>
              </View>
            </View>
          )
        }}/>

      
                  
    </View>
  );
  }else{
    return (
      <View style={styles.loading}>
        <ActivityIndicator size="large" color="#00ff00" />
      </View>
    )
  }
  
}

search = text => {
  console.log(text);
};
clear = () => {
  this.search.clear();
};

}

 
import React, {Component} from 'react'
import { connect } from 'react-redux'
import {View, Text,TouchableOpacity, Platform, StyleSheet } from 'react-native'
import { Icon } from 'react-native-elements'
import  firebase  from '../../config/firebase'
import { logOut } from '../../redux/actions/userActions'

class HeaderCustom extends Component{
    constructor(props){
        super(props)
    }

    logOut = () => {
        firebase.auth().signOut();
        this.props.logOut();
        this.props.props.navigation.navigate('Menu')
    }

render(){
    if(this.props.data == 'logout'){
        return(
            <TouchableOpacity onPress={this.logOut.bind(this)}>
                    <Icon name={this.props.icon} color="white"></Icon>
                    <Text style={{color:'white', fontSize: 12}}>{this.props.title}</Text>
                </TouchableOpacity>
            
        )
    }else if(this.props.data == 'title'){
        return(
            <View>
            <Text style={{color:'white', fontSize: 20}}>{this.props.title}</Text>
          </View>
            
        )
    }else if(this.props.data == 'left'){
        return(
            <View>
                <TouchableOpacity onPress={this._link.bind(this)}>
                    <Icon name={this.props.icon} color="white"></Icon>
                </TouchableOpacity>
          </View>
            
        )
    }else{
        return(
            <View>
                
          </View>
            
        )
    }
       
}
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red',
        justifyContent: 'space-around',
        flexDirection: 'row',
        marginTop: 50,
        height:50,
        width:'100%'
    }
    
  }
);


function mapStateToProps(state) {
    return {
      user: state.user
    }
  }
  
  export default connect(mapStateToProps, {logOut })(HeaderCustom);
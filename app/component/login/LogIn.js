import React, { Component } from 'react';
import { connect } from 'react-redux'
import { View,  Image,  TextInput,  TouchableOpacity,  Text, Alert, AsyncStorage,Animated, Dimensions, Keyboard, UIManager,TouchableWithoutFeedback  } from 'react-native';
import styles from "./style";

import { fetchUser, setUser } from '../../redux/actions/userActions'
import userService from '../../services/userServices'
import store from '../../redux/store'

const { State: TextInputState } = TextInput;

class LogIn extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
        route: 'Login',
        username: '',
        password: '',
        loader: false,
        shift: new Animated.Value(0)
    };

    this.authenticate();
}

loginToFireBase() {
  this.setState({ loader: true });
  userService.userLogin(this.state.username.trim(), this.state.password)
    .then(user => {
      let email = user.user.email
      var code = email.substring(0, email.indexOf("@"));
      code = Number(code);
      let isInteger = Number.isInteger(code);
      if(isInteger){
        userService.getUserAuthenticate(code)
      .then((responseJson) => {
         this.props.setUser(responseJson);
         this.setState({ loader: false });
         this.props.navigation.navigate('SignedIn');
      });
      }else{
        this.setState({ loader: false });
        Alert.alert('Alerta','Usuario o Clave incorrecta!');
      }
      
    }).catch(error => {
      this.setState({ loader: false });
      Alert.alert('Alerta',error.toString());
      
    });
};
authenticate() {

  AsyncStorage.getItem('CURRENT_USER')
      .then((user) => {
        if(user == null){
          this.props.navigation.navigate('Inicio');
        }else{
          this.props.navigation.navigate('Menu');
        }        
      }).catch(() => {
        Alert.alert('Alert','Usuario o Clave incorrecta!');
      });
}

  _getDisabled(){
    let disabled = false;
    if(this.state.loader)
        disabled = true;
    return disabled;
}
  

  render() {
    
    return (
<Animated.View style={[styles.container, { transform: [{translateY: this.state.shift}] }]}>
<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <View style={styles.container}>
        <View style={{ flex: 1 }} >
          <Image resizeMode="cover" style={[ { width: '100%', height: '100%', overflow: 'visible'} ]}
            source={{uri:"https://www.gcsac.com.pe/wp-content/uploads/2017/09/balde-pato.jpg"}}
          />
        </View>
        <TextInput placeholder="Ingrese correo" style={[styles.textInput, { marginTop: 40 }]} 
        onChange={(event) => this.setState({username: event.nativeEvent.text})}
        returnKeyType = { "next" }
        autoFocus = {true}
        value={this.state.username}
        ref={ref => {
          this._emailinput = ref;
        }}
        onSubmitEditing={() =>
          this._passwordinput && this._passwordinput.focus()
        }
        />
        <TextInput 
        ref={ref => {
          this._passwordinput = ref;
        }}
        placeholder="Ingrese contraseña" secureTextEntry={true} style={[styles.textInput, { marginVertical: 20 }]} 
        onChange={(event) => this.setState({password: event.nativeEvent.text})}
        value={this.state.password}
        />

        <TouchableOpacity disabled={this._getDisabled()} onPress={this.loginToFireBase.bind(this)} 
        style={[styles.button]} 
        >
          <Text style={{ color: 'white', fontSize: 20, fontWeight: '600' }}>
          { (this.state.loader ? 'Cargando...' : 'INICIAR')}
          </Text>
        </TouchableOpacity>
      </View>
      </TouchableWithoutFeedback>
      </Animated.View>
    );
  }

  componentWillMount() {
    this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
    this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide);
  }
  
  componentWillUnmount() {
    this.keyboardDidShowSub.remove();
    this.keyboardDidHideSub.remove();
  }

  handleKeyboardDidShow = (event) => {
    const { height: windowHeight } = Dimensions.get('window');
    const keyboardHeight = event.endCoordinates.height;
    const currentlyFocusedField = TextInputState.currentlyFocusedField();
    UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
      const fieldHeight = height;
      const fieldTop = pageY;
      const gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight);
      if (gap >= 0) {
        return;
      }
      Animated.timing(
        this.state.shift,
        {
          toValue: gap,
          duration: 1000,
          useNativeDriver: true,
        }
      ).start();
    });
  }

  handleKeyboardDidHide = () => {
    Animated.timing(
      this.state.shift,
      {
        toValue: 0,
        duration: 1000,
        useNativeDriver: true,
      }
    ).start();
  }
}

function mapStateToProps(state) {
  return {
    user: state.user
  }
}

export default connect(mapStateToProps, {setUser, fetchUser })(LogIn);